//
//  LKD8BaseImg.h
//  LKD8
//
//  Created by Nuo A on 2020/9/23.
//  Copyright © 2020 cjf. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LKD8BaseImg : NSObject

+ (UIImage *)zlc_imageNamed:(NSString *)name ofType:(nullable NSString *)type;

@end

NS_ASSUME_NONNULL_END
