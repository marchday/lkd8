//
//  LKD8Base.h
//  LKD8
//
//  Created by Nuo A on 2020/9/22.
//  Copyright © 2020 cjf. All rights reserved.
//

#import "SDWebImage.h"

#import "NetWorkManager.h"
#import "MBProgressHUD+SGExtension.h"

#import "LKD8BaseNavViewController.h"
#import "LKD8HomeViewController.h"
#import "CourtListTableViewCell.h"
#import "CourtDateilsViewController.h"
#import "LKD8BaseImg.h"

#ifndef LKD8Base_h
#define LKD8Base_h

//获取屏幕宽高
#define kScreenWidth [UIScreen mainScreen].bounds.size.width
#define kScreenHight [UIScreen mainScreen].bounds.size.height
//适配屏幕
#define kScale kScreenWidth/375.0
#define kScaleH  (kScreenHight/667.0)

#define RGBColor(r, g, b) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1.0]
#define ColorAlpha(r,g,b,a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]


// 状态栏高度
#define statusbarHeight_forTools [[UIApplication sharedApplication] statusBarFrame].size.height

#define kTabBarHeight (statusbarHeight_forTools > 20 ? 83 : 49)
#endif /* LKD8Base_h */
