
#import "MBProgressHUD.h"

@interface MBProgressHUD (SGExtension)

+ (void)showSuccess:(NSString *)success;
+ (void)showError:(NSString *)error;
+ (void)showMessage:(NSString *)message toView:(UIView *)view;
+ (void)showMessage:(NSString *)message;
+ (void)hideHUDForView:(UIView *)view;
+ (void)showHudWithTitle:(NSString *)title onView:(UIView *)view;
+ (void)hideHUD;

@end
