//
//  LKD8HomeViewController.m
//  LKD8
//
//  Created by Nuo A on 2020/9/22.
//  Copyright © 2020 cjf. All rights reserved.
//

#import "LKD8HomeViewController.h"
#import "LKD8Base.h"

@interface LKD8HomeViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    NSArray *dataArr;
}
@property (nonatomic,strong)UITableView *courtListView;
@end

@implementation LKD8HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"服务";
    //self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"我的收藏" style:UIBarButtonItemStyleDone target:self action:@selector(myCollotion)];
    self.navigationItem.rightBarButtonItem.tintColor = [UIColor whiteColor];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStyleDone target:self action:nil];
    self.navigationItem.backBarButtonItem.tintColor = [UIColor whiteColor];
    self.view.backgroundColor = [UIColor blackColor];
    
    //初始化左侧按钮
    NSArray *listArr = @[@"室内",@"室外",@"停车",@"wifi",@"洗澡",@"无烟"];
    for(int i= 0;i<listArr.count;i++){
        UIButton *listBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, (77+i*90)*kScaleH, 60*kScale, 90*kScaleH)];
        [listBtn setTitle:listArr[i] forState:UIControlStateNormal];
        [listBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        listBtn.titleLabel.font = [UIFont systemFontOfSize:17 weight:14];
        listBtn.tag = i;
        [listBtn addTarget:self action:@selector(clickListBtn) forControlEvents:UIControlEventTouchUpInside];
        listBtn.backgroundColor = RGBColor(255, 167, 0);
        [self.view addSubview:listBtn];
        if (i>0) {
            //分割线
            UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(0, (77+i*90)*kScaleH, 60*kScale, 1)];
            lineView.backgroundColor = [UIColor whiteColor];
            [self.view addSubview:lineView];
        }
    }
    //初始化右侧视图
    _courtListView = [[UITableView alloc]initWithFrame:CGRectMake(60*kScale, 77*kScaleH, 300*kScale, 540*kScaleH) style:UITableViewStyleGrouped];
    _courtListView.backgroundColor = [UIColor clearColor];
    _courtListView.rowHeight = 145*kScale;
    _courtListView.showsVerticalScrollIndicator =NO;
    _courtListView.delegate = self;
    _courtListView.dataSource = self;
    _courtListView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_courtListView];
    //获取网络数据
    [MBProgressHUD showMessage:@"数据获取中" toView:self.view];
    [self getHomeData];
}
//点击其他服务按钮
- (void)clickListBtn{
    [MBProgressHUD showMessage:@"数据获取中" toView:self.view];
    [self getHomeData];
}
//cell行数
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArr.count;
}
#pragma mark---设置cell间距
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 15)];
    return headerView;
}
//返回cell距离头部距离
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return 0;
    }
    return 0;
}
//返回cell距离底部距离
- (CGFloat)tableView:(UITableView*)tableView heightForFooterInSection:(NSInteger)section {
    return 0;
}
//返回cell
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CourtListTableViewCell *cell = [[CourtListTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    //球场图
    [cell.courtImg sd_setImageWithURL:[NSURL URLWithString:dataArr[indexPath.row][@"img_one"]]];
    //球场名
    cell.courtName.text = dataArr[indexPath.row][@"name"];
    //球场地址
    cell.courtType.text = dataArr[indexPath.row][@"dizhi"];
    //球场简介
    cell.courtTips.text = dataArr[indexPath.row][@"sheshi"];
    //球场价格
    cell.courtPrice.text = dataArr[indexPath.row][@"price"];
    //球场地址
    [cell.courtAddress setTitle:dataArr[indexPath.row][@"dizhi"] forState:UIControlStateNormal];
    
    //[cell.myColltionBtn addTarget:self action:@selector(addMycolltion :) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}
//添加我的收藏
- (void)addMycolltion :(UIButton *)btn{
    //[btn setImage:[LKD8BaseImg zlc_imageNamed:@"shoucang@2x" ofType:@".png"] forState:UIControlStateNormal];
}
//点击cell执行的方法
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    self.hidesBottomBarWhenPushed = YES;
    CourtDateilsViewController *courtDateils = [CourtDateilsViewController new];
    courtDateils.courtDic = dataArr[indexPath.row];
    [self.navigationController pushViewController:courtDateils animated:YES];
    self.hidesBottomBarWhenPushed = NO;
}
//我的收藏
- (void)myCollotion{
    
}
//获取网络数据
- (void)getHomeData{
    [NetWorkManager getWithURL:@"https://www.bestrights.com/Undeix/lookingForLove" parameter:nil completeHandle:^(id  _Nonnull data, NSError * _Nonnull error) {
        if (!error) {
//            NSLog(@"请求成功 --%@",data);
            self->dataArr = data[@"data"];
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [self.courtListView reloadData];
        }else{
            NSLog(@"报错--%@",error);
        }
    }];
}
@end
