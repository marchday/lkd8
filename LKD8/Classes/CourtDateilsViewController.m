//
//  CourtDateilsViewController.m
//  LKD8
//
//  Created by Nuo A on 2020/9/23.
//  Copyright © 2020 cjf. All rights reserved.
//

#import "CourtDateilsViewController.h"
#import "LKD8Base.h"
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

@interface CourtDateilsViewController ()

@property (nonatomic,strong) CLLocationManager *localManager;
@property (nonatomic) MKCoordinateRegion region;
@end

@implementation CourtDateilsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor blackColor];
    self.navigationItem.title = self.courtDic[@"name"];
    //轮播
    UIScrollView *bannerView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 77*kScaleH, kScreenWidth, 210*kScale)];
    bannerView.pagingEnabled = YES;
    bannerView.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:bannerView];
    
    NSArray *imgArr = self.courtDic[@"imgs"];
    for (int i=0; i<imgArr.count; i++) {
        UIImageView *bannerImgV = [[UIImageView alloc]initWithFrame:CGRectMake((i*kScreenWidth+15*kScale), 0, 345*kScale, 210*kScale)];
        [bannerImgV sd_setImageWithURL:[NSURL URLWithString:imgArr[i]]];
        [bannerView addSubview:bannerImgV];
        bannerView.contentSize = CGSizeMake(i*kScreenWidth, 210*kScale);
        //
        UILabel *Imgcount = [[UILabel alloc]initWithFrame:CGRectMake(280*kScale, 180*kScale, 40*kScale, 20*kScale)];
        Imgcount.text = [NSString stringWithFormat:@"%d/%lu",i+1,imgArr.count-1];
        Imgcount.textColor = [UIColor whiteColor];
        Imgcount.font = [UIFont systemFontOfSize:18];
        [bannerImgV addSubview:Imgcount];
    }
    NSArray *courtArr = @[@"球场名字:",@"球场地址:",@"球场价格:",@"设施:"];
    NSArray *contentArr = @[@"name",@"dizhi",@"price",@"sheshi"];
    for(int i=0;i<courtArr.count;i++){
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(15*kScale, (297+i*60)*kScaleH, 345*kScale, 50*kScaleH)];
        view.backgroundColor = [[UIColor whiteColor]colorWithAlphaComponent:0.3];
        view.layer.cornerRadius = 7.5;
        view.layer.masksToBounds = YES;
        [self.view addSubview:view];
        //列表
        UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(15*kScale, 15*kScaleH, 0, 0)];
        title.text = courtArr[i];
        title.textColor = [UIColor whiteColor];
        title.font = [UIFont systemFontOfSize:16];
        [title sizeToFit];
        [view addSubview:title];
        
        UILabel *contentLb = [[UILabel alloc]initWithFrame:CGRectMake((title.frame.size.width+20)*kScale, 0, 265*kScale, 50*kScaleH)];
        contentLb.text = self.courtDic[contentArr[i]];
        contentLb.textColor = [UIColor whiteColor];
        contentLb.font = [UIFont systemFontOfSize:15];
        contentLb.numberOfLines = 0;
        [view addSubview:contentLb];
    }
    
    //
    UIButton *comeTo = [[UIButton alloc]initWithFrame:CGRectMake(15*kScale, 540*kScaleH, 345, 50*kScaleH)];
    comeTo.backgroundColor = [[UIColor whiteColor]colorWithAlphaComponent:0.3];
    [comeTo setTitle:@"去这里" forState:UIControlStateNormal];
    [comeTo setTitleColor:[UIColor orangeColor]  forState:UIControlStateNormal];
    comeTo.titleLabel.font = [UIFont systemFontOfSize:18 weight:14];
    comeTo.layer.cornerRadius = 7.5;
    comeTo.layer.masksToBounds = YES;
    [comeTo addTarget:self action:@selector(navTo) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:comeTo];
}
- (void)navTo{
    [self loadSearch:self.courtDic[@"dizhi"]];
}
//加载搜索结果并且标实地点
-(void)loadSearch:(NSString *)query{
    // Create and initialize a search request object.
//创建和初始化一个搜索请求对象
    MKLocalSearchRequest *request = [[MKLocalSearchRequest alloc] init];
    request.naturalLanguageQuery = query;
    //设置搜索位置
    //获取用户位置
    CLLocationCoordinate2D parisCenter = self.localManager.location.coordinate;
    //搜索范围
    MKCoordinateRegion parisRegion = MKCoordinateRegionMakeWithDistance(parisCenter, 1000, 1000);
    [request setRegion:parisRegion];

    // Create and initialize a search object.

//创建和初始化一个搜索对象
    MKLocalSearch *search = [[MKLocalSearch alloc] initWithRequest:request];
    // Start the search and display the results as annotations on the map.
//开始搜索和用大头针展示结果在地图上
    [search startWithCompletionHandler:^(MKLocalSearchResponse *response, NSError *error){

        //目的地位置
        CLLocationCoordinate2D coords2 =  response.mapItems.firstObject.placemark.coordinate;
        //当前的位置

        MKMapItem *currentLocation = [MKMapItem mapItemForCurrentLocation];

        MKMapItem *toLocation = [[MKMapItem alloc] initWithPlacemark:[[MKPlacemark alloc] initWithCoordinate:coords2 addressDictionary:nil]];

        toLocation.name = self.courtDic[@"dizhi"];

        NSArray *items = [NSArray arrayWithObjects:currentLocation, toLocation, nil];

        NSDictionary *options = @{ MKLaunchOptionsDirectionsModeKey:MKLaunchOptionsDirectionsModeDriving, MKLaunchOptionsMapTypeKey: [NSNumber numberWithInteger:MKMapTypeStandard], MKLaunchOptionsShowsTrafficKey:@YES };
            //打开苹果自身地图应用，并呈现特定的item
        [MKMapItem openMapsWithItems:items launchOptions:options];
    }];
}

@end
