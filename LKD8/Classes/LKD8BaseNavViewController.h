//
//  LKD8BaseNavViewController.h
//  LKD8
//
//  Created by Nuo A on 2020/9/22.
//  Copyright © 2020 cjf. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LKD8BaseNavViewController : UINavigationController

@end

NS_ASSUME_NONNULL_END
