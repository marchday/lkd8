//
//  LKD8BaseNavViewController.m
//  LKD8
//
//  Created by Nuo A on 2020/9/22.
//  Copyright © 2020 cjf. All rights reserved.
//

#import "LKD8BaseNavViewController.h"
#import "LKD8Base.h"

@interface LKD8BaseNavViewController ()

@end

@implementation LKD8BaseNavViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //去除导航下面的黑边
    self.navigationBar.shadowImage = [UIImage new];
    //将导航背景设置为空
    [self.navigationBar setBackgroundImage:[UIImage new] forBarMetrics: UIBarMetricsDefault];
    //设置导航文字颜色
    [self.navigationBar setTitleTextAttributes:@{
        NSForegroundColorAttributeName:[UIColor whiteColor],
        NSFontAttributeName:[UIFont systemFontOfSize:17 weight:13],
    }];
    [self.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]} forState:UIControlStateNormal];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
