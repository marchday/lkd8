//
//  XLCycleCell.m
//  XLCycleCollectionViewDemo
//
//  Created by MengXianLiang on 2017/3/6.
//  Copyright © 2017年 MengXianLiang. All rights reserved.
//

#import "XLCycleCell.h"
#import "LKD8Base.h"

@interface XLCycleCell ()

@property (nonatomic, strong) UIImageView *UIimg;

@end

@implementation XLCycleCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self buildUI];
    }
    return self;
}

- (void)buildUI {
    self.UIimg = [[UIImageView alloc] initWithFrame:CGRectMake(15*kScale, 46*kScaleH, 345*kScale, 150*kScaleH)];
    [self addSubview:self.UIimg];
}

- (void)setTitle:(NSString *)title {
    self.UIimg.image =[UIImage imageNamed:title];
}

@end
